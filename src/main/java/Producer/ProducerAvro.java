package Producer;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class ProducerAvro {

    public static String TOPIC = "user-list";

    private Properties kafkaProperties;
    private KafkaProducer <GenericRecord, GenericRecord> producer;
    private Schema valueSchema;
    private GenericRecord valueRecord;
    private ProducerRecord<GenericRecord, GenericRecord> record;
    private Connection connection;

    public ProducerAvro(Connection connection) {
        this.connection = connection;
        initProperties();
        record();
    }

    public void initProperties(){
        kafkaProperties = new Properties();
        kafkaProperties.put("bootstrap.servers", "localhost:9092");
        kafkaProperties.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProperties.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProperties.put("schema.registry.url","http://localhost:8081");
        kafkaProperties.put("acks", "all");
    }

    public void record(){
        producer = new KafkaProducer<GenericRecord, GenericRecord>(kafkaProperties);
        valueSchema = SchemaBuilder.record("ValueRecord")
                .fields()
                .name("id").type().intType().noDefault()
                .name("first_name").type().stringType().noDefault()
                .name("last_name").type().stringType().noDefault()
                .name("email").type().stringType().noDefault()
                .name("gender").type().stringType().noDefault()
                .name("ip_address").type().stringType().noDefault()
                .endRecord();

        /*try {
            valueSchema = new Schema.Parser().parse(new File("/home/alduin/IdeaProjects/kafkaAvro/src/main/resources/schema.avsc"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        String query = "SELECT * FROM users";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                valueRecord = new GenericData.Record(valueSchema);
                valueRecord.put("id", resultSet.getInt("id"));
                valueRecord.put("first_name", resultSet.getString("first_name") +" ");
                valueRecord.put("last_name", resultSet.getString("last_name") + " ");
                valueRecord.put("email", resultSet.getString("email")+" ");
                valueRecord.put("gender", resultSet.getString("gender")+" ");
                valueRecord.put("ip_address", resultSet.getString("ip_address") + " ");
                record = new ProducerRecord<GenericRecord, GenericRecord>(TOPIC, null, valueRecord);
                producer.send(record);
                //Thread.sleep(100);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}