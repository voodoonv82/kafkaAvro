import Consumer.ConsumerAvro;
import Producer.ProducerAvro;

import java.sql.Connection;
import java.sql.DriverManager;


public class Main {

    static String USER = "root";
    static String PASSWORD = "Mustangmach1";

    static String DRIVER = "com.mysql.jdbc.Driver";
    static String DATABASE_URL = "jdbc:mysql://localhost/kafka_message";

    public static void main(String[] args) {
        Connection connection;

        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
            new ConsumerAvro(connection);
            new ProducerAvro(connection);
        }catch (Exception e){
            System.err.println(e.getMessage());
        }

    }
}
