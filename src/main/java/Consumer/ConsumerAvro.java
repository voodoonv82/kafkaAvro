package Consumer;

import Producer.ProducerAvro;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Collections;
import java.util.Properties;

public class ConsumerAvro {

    private Properties kafkaProperties;
    private KafkaConsumer consumer;
    private ConsumerRecords <GenericRecord, GenericRecord> records;
    private Connection connection;

    public ConsumerAvro(Connection connection) {
        this.connection = connection;
        initProperties();
        reader();
    }

    public void initProperties(){
        kafkaProperties = new Properties();
        kafkaProperties.put("bootstrap.servers", "localhost:9092");
        kafkaProperties.put("group.id", "new-group");
        kafkaProperties.put("key.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        kafkaProperties.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        kafkaProperties.put("schema.registry.url","http://localhost:8081");
    }

    public void reader(){

        consumer = new KafkaConsumer<GenericRecord, GenericRecord>(kafkaProperties);
        consumer.subscribe(Collections.singletonList(ProducerAvro.TOPIC));
        try {
            Statement statement = connection.createStatement();
            while (true) {
                records = consumer.poll(1000);
                String genderTable;
                GenericRecord genericRecord;

                for (ConsumerRecord<GenericRecord, GenericRecord> record : records) {
                    genericRecord = record.value();
                    if (getValue(genericRecord, "gender", String.class).contains("Female")){
                        genderTable = "female";
                    }else genderTable = "male";
                    statement.executeUpdate("INSERT INTO " + genderTable + "(id, first_name, last_name, email, ip_address) VALUES("
                            +getValue(genericRecord, "id", Integer.class)
                            +",'"+getValue(genericRecord, "first_name", String.class)+"',"
                            +"'"+getValue(genericRecord, "last_name", String.class)
                            +"','"+getValue(genericRecord, "email", String.class)
                            +"','"+getValue(genericRecord, "ip_address", String.class)+"')"
                    );
                }
                consumer.commitSync();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static <T> T getValue(GenericRecord genericRecord, String name, Class<T> classs)
    {
        Object obj = genericRecord.get(name);
        if (obj == null)
            return null;
        if (obj.getClass() == Utf8.class)
        {
            return (T) obj.toString();
        }
        if (obj.getClass() ==  Integer.class)
        {
            return (T) obj;
        }
        return null;
    }
}
